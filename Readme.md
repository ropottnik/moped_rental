# Moped Store Chatbot

## Introduction
This Project consists of a basic chatbot for messenger using a simple NLP processing engine that is 
trained using Tensorflow. 

## Components

The whole functionality of the bot can be broken down into four parts

1) Model training and exporting routine `model_train.py`
 * This script reads out the file `intents.json` and produces a neural network using Keras on top
 of Tensorflow. The model is then exported into `saved_models`. The resulting model takes in
 vectors in terms of the underlying vocabulary and returns vectors of predictions in therms of 
 the specified intents. 
    
2) Tensorflow Model Server
 * According to www.tensorflow.org/serving/​ a tensorflow server is set up 
 acting as an RPC service which can be fed with word vectors. The word vectors need to have 
 the correct dimension in terms of the vocabulary of the dataset which can be found in `intents.json`

3) Flask Text Processing Server
 * This server acts as a proxy for the Tensorflow Model Server and converts text which 
 is sent to its endpoint into the appropriate vectors and sents these to the Tensorflow Model Server. 
 It then converts the result vector into intent tags corresponding to `intents.json`

4) Chatbot and Moped Store Website
 * The chatbot is build using the `botkit` framework. It passes text on to the Flask Server and 
 handles resulting intents based on the rules specified in `intents.json`. It incorporates
 a handover mechanism that can be triggered either by intent or with a postback that is supplied
 with each response of the bot. 
 
 * On route `/` of the bot's domain the user can access the index page which displays a messenger
 window and the customer chat plugin (beta).
    
## Production

### Prerequisites
* Facebook Page with activated handover protocol
* Facebook App
* Docker Compose

### Running the Bot
After filling out all the required details inside `docker-compose.json`, the required services are
started with
```bash
docker-compose up
```

The Bot is then served on port 3000