"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Intent = (function () {
    function Intent(rawIntent) {
        this.rawIntent = rawIntent;
    }
    Intent.prototype.getRandomResponse = function () {
        return this.rawIntent.responses[Math.floor(Math.random() * this.rawIntent.responses.length)];
    };
    Object.defineProperty(Intent.prototype, "context_set", {
        get: function () {
            return this.rawIntent.context_set;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Intent.prototype, "context_filter", {
        get: function () {
            return this.rawIntent.context_filter;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Intent.prototype, "tag", {
        get: function () {
            return this.rawIntent.tag;
        },
        enumerable: true,
        configurable: true
    });
    return Intent;
}());
exports.Intent = Intent;
