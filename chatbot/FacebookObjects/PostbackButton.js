"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PostbackButton = (function () {
    function PostbackButton(text, payload) {
        this.text = text;
        this.payload = payload;
    }
    PostbackButton.prototype.resolve = function () {
        return {
            type: "postback",
            title: this.text,
            payload: this.payload
        };
    };
    return PostbackButton;
}());
exports.PostbackButton = PostbackButton;
