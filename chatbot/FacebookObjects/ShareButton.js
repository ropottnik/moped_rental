"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ShareButton = (function () {
    function ShareButton() {
    }
    ShareButton.prototype.resolve = function () {
        return {
            type: 'element_share'
        };
    };
    return ShareButton;
}());
exports.ShareButton = ShareButton;
