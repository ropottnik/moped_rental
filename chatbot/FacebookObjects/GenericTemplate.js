"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GenericTemplate = (function () {
    function GenericTemplate(text, buttons) {
        this.buttons = buttons;
        this.text = text;
    }
    GenericTemplate.prototype.resolve = function () {
        return {
            type: "template",
            payload: {
                template_type: "generic",
                elements: [
                    {
                        title: this.text,
                        buttons: this.buttons.map(function (button) { return button.resolve(); })
                    }
                ]
            }
        };
    };
    return GenericTemplate;
}());
exports.GenericTemplate = GenericTemplate;
