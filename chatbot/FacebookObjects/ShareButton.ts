import {FacebookObject} from "./FacebookObject";

export class ShareButton implements FacebookObject {
    resolve() {
        return {
            type: 'element_share'
        }
    }
}