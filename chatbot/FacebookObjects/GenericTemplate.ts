import {FacebookObject} from "./FacebookObject";
import {FacebookAttachment} from "botkit";

export class GenericTemplate implements FacebookObject{
    buttons: FacebookObject[];
    text: string;

    constructor(text: string, buttons: FacebookObject[]) {
        this.buttons = buttons;
        this.text = text;
    }

    resolve(): FacebookAttachment {
        return {
            type: "template",
            payload: {
                template_type: "generic",
                elements: [
                    {
                        title: this.text,
                        buttons: this.buttons.map(button => button.resolve())
                    }
                ]

            }
        }
    }
}