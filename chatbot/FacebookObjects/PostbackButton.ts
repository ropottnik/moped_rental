import {FacebookObject} from "./FacebookObject";

export class PostbackButton implements FacebookObject {
    text: string;
    payload: string;

    constructor(text: string, payload: string) {
        this.text = text;
        this.payload = payload;
    }

    resolve() {
        return {
            type: "postback",
            title: this.text,
            payload: this.payload
        }
    }
}