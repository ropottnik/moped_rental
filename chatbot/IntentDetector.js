"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var Intent_1 = require("./Intent");
var IntentDetector = (function () {
    function IntentDetector(intentsFilePath) {
        this.context = {};
        this.intents = JSON.parse(fs.readFileSync(intentsFilePath, "utf8")).intents;
    }
    IntentDetector.prototype.detectIntentForUserAndHandleContext = function (userId, intentList) {
        if (intentList) {
            // loop as long as there are matches to process
            for (var _i = 0, intentList_1 = intentList; _i < intentList_1.length; _i++) {
                var nextBestCandidate = intentList_1[_i];
                var intent = this.getIntentForTag(nextBestCandidate.tag);
                // set context for this tag if necessary
                if (intent.context_set || intent.context_set === "") {
                    this.context[userId] = intent.context_set;
                }
                if (!intent.context_filter || (this.context[userId] && intent.context_filter === this.context[userId])) {
                    // a random response from the tag
                    // return tag.responses[Math.floor(Math.random()*tag.responses.length)];
                    return intent;
                }
            }
        }
        return null;
    };
    IntentDetector.prototype.getIntentForTag = function (tag) {
        for (var _i = 0, _a = this.intents; _i < _a.length; _i++) {
            var intent = _a[_i];
            if (tag === intent.tag) {
                return new Intent_1.Intent(intent);
            }
        }
    };
    return IntentDetector;
}());
exports.IntentDetector = IntentDetector;
