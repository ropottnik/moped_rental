'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var dotenv = require("dotenv");
var Botkit = require("botkit");
// import { FacebookMessage } from './Interfaces/FacebookInterfaces';
var IntentServerConnector_1 = require("./IntentServerConnector");
var IntentDetector_1 = require("./IntentDetector");
var FacebookConnector_1 = require("./FacebookConnector");
var GenericTemplate_1 = require("./FacebookObjects/GenericTemplate");
var PostbackButton_1 = require("./FacebookObjects/PostbackButton");
var ShareButton_1 = require("./FacebookObjects/ShareButton");
var path = require("path");
dotenv.load();
var intentServerConnector = new IntentServerConnector_1.IntentServerConnector(process.env.INTENT_RECOGNITION_HOST, process.env.INTENT_RECOGNITION_PORT);
var intentDetector = new IntentDetector_1.IntentDetector(process.env.INTENTS_FILE_PATH);
var facebookConnector = new FacebookConnector_1.FacebookConnector(process.env.SECONDARY_APP_ID, process.env.FACEBOOK_PAGE_ACCESS_TOKEN);
var controller = Botkit.facebookbot({
    access_token: process.env.FACEBOOK_PAGE_ACCESS_TOKEN,
    verify_token: process.env.FACEBOOK_VERIFY_TOKEN
});
var bot = controller.spawn({});
controller.setupWebserver(process.env.BOT_PORT, function (err, webserver) {
    controller.createWebhookEndpoints(controller.webserver, bot, function () {
        console.log('This bot is online!!!');
    });
});
controller.hears(['.*'], 'message_received', function (bot, message) {
    return __awaiter(this, void 0, void 0, function () {
        var intentServerResopnse, intent;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, intentServerConnector.getIntents(message.text)];
                case 1:
                    intentServerResopnse = _a.sent();
                    intent = intentDetector.detectIntentForUserAndHandleContext(message.user, intentServerResopnse);
                    if (!(intent.tag === 'humanhandover')) return [3 /*break*/, 3];
                    return [4 /*yield*/, facebookConnector.handOverToSecondaryApp(message.user)];
                case 2:
                    _a.sent();
                    bot.reply(message, intent.getRandomResponse());
                    return [3 /*break*/, 4];
                case 3:
                    bot.reply(message, getGenericResponse(intent.getRandomResponse()));
                    _a.label = 4;
                case 4: return [2 /*return*/];
            }
        });
    });
});
controller.on('facebook_postback', function (bot, message) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            if (message.payload === 'humanhandover') {
                facebookConnector.handOverToSecondaryApp(message.user);
                bot.reply(message, intentDetector.getIntentForTag(message.payload).getRandomResponse());
            }
            return [2 /*return*/];
        });
    });
});
controller.on('facebook_referral', function (bot, message) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            if (message.referral.ref === 'morningslot' || message.referral.ref === 'afternoonslot') {
                bot.reply(message, getGenericResponse(intentDetector.getIntentForTag(message.referral.ref).getRandomResponse()));
            }
            return [2 /*return*/];
        });
    });
});
// serve the website for the moped store
controller.webserver.set("views", path.join(__dirname, "/views"));
controller.webserver.set("view engine", "pug");
controller.webserver.get("/", function (req, res) {
    res.render('index', {
        page_id: process.env.FACEBOOK_PAGE_ID
    });
});
// create a response that renders to generic template in facebook
function getGenericResponse(text) {
    return {
        notification_type: 'REGULAR',
        attachment: new GenericTemplate_1.GenericTemplate(text, [new PostbackButton_1.PostbackButton('Customer Agent', 'humanhandover'), new ShareButton_1.ShareButton]).resolve()
    };
}
// handle basic setup for messenger plugin (beta)
facebookConnector.whiteListURIs([process.env.WEBSITE_URI]);
