export interface IntentCandidate {
    tag: string,
    score: number
}