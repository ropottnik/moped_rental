export interface RawIntent {
    tag: string,
    responses: string[],
    context_set: string,
    context_filter: string
}