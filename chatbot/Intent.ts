import { RawIntent } from './Interfaces/RawIntent'

export class Intent {
    rawIntent: RawIntent

    constructor(rawIntent: RawIntent) {
        this.rawIntent = rawIntent;
    }

    getRandomResponse() : string {
        return this.rawIntent.responses[Math.floor(Math.random()*this.rawIntent.responses.length)];
    }

    get context_set(): string{
        return this.rawIntent.context_set;
    }

    get context_filter(): string{
        return this.rawIntent.context_filter;
    }

    get tag(): string{
        return this.rawIntent.tag;
    }
}