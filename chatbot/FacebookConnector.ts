'use strict';
import axios from 'axios';

export class FacebookConnector {
    secondaryAppId: string;
    pageAccessToken: string;
    facebookGraphUrl : string = "https://graph.facebook.com/v2.6/me";

    constructor (secondaryAppId: string, pageAccessToken: string) {
        this.secondaryAppId = secondaryAppId;
        this.pageAccessToken = pageAccessToken;
    }

    async handOverToSecondaryApp(senderId: string) : Promise<void> {
        await this.basicRequest('/pass_thread_control', {
            recipient: {
                id: senderId
            },
            target_app_id: this.secondaryAppId,
            metadata: "String to pass to secondary receiver app"
        })
    }

    async takeBackFromSecondaryApp(senderId: string) : Promise<void> {
        await this.basicRequest('/take_thread_control', {
            recipient: {
                id: senderId
            },
            metadata: "String to pass to secondary receiver app"
        })
    }

    async whiteListURIs(uris: string[]) : Promise<void> {
        await axios.post(`https://graph.facebook.com/v2.10/me/messenger_profile?access_token=${process.env.FACEBOOK_PAGE_ACCESS_TOKEN}`,
                {
                    "setting_type": "domain_whitelisting",
                    "whitelisted_domains": [process.env.WEBSITE_URI],
                    "domain_action_type": "add"
                }
            );
    }

    async basicRequest(route: string, data: any) : Promise<void> {
        await axios.post(`${this.facebookGraphUrl}${route}?access_token=${this.pageAccessToken}`, data);
    }
}