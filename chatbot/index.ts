'use strict';
import * as dotenv from "dotenv";
import * as Botkit from 'botkit';
// import { FacebookMessage } from './Interfaces/FacebookInterfaces';
import { IntentServerConnector } from './IntentServerConnector';
import { IntentDetector } from './IntentDetector';
import {Intent} from "./Intent";
import {IntentCandidate} from "./Interfaces/IntentServerResponse";
import {FacebookConnector} from "./FacebookConnector";
import {GenericTemplate} from "./FacebookObjects/GenericTemplate";
import {PostbackButton} from "./FacebookObjects/PostbackButton";
import {ShareButton} from "./FacebookObjects/ShareButton";
import {FacebookAttachment, FacebookBot, FacebookMessage} from "botkit";
import * as path from "path";

dotenv.load();

const intentServerConnector = new IntentServerConnector(process.env.INTENT_RECOGNITION_HOST, process.env.INTENT_RECOGNITION_PORT);
const intentDetector = new IntentDetector(process.env.INTENTS_FILE_PATH);
const facebookConnector = new FacebookConnector(process.env.SECONDARY_APP_ID, process.env.FACEBOOK_PAGE_ACCESS_TOKEN);

const controller = Botkit.facebookbot({
    access_token: process.env.FACEBOOK_PAGE_ACCESS_TOKEN,
    verify_token: process.env.FACEBOOK_VERIFY_TOKEN
});

const bot = controller.spawn({});

controller.setupWebserver(process.env.BOT_PORT ,function(err, webserver) {
    controller.createWebhookEndpoints(controller.webserver, bot, function() {
        console.log('This bot is online!!!');
    });
});

controller.hears(['.*'], 'message_received', async function(bot: FacebookBot, message: FacebookMessage) {
    // console.log(message);
    const intentServerResopnse : IntentCandidate[] = await intentServerConnector.getIntents(message.text);
    const intent : Intent = intentDetector.detectIntentForUserAndHandleContext(message.user, intentServerResopnse);

    if (intent.tag === 'humanhandover') {
        await facebookConnector.handOverToSecondaryApp(message.user);
        bot.reply(message, intent.getRandomResponse());
    } else {
        bot.reply(message, getGenericResponse(intent.getRandomResponse()));
    }
});

controller.on('facebook_postback', async function (bot: FacebookBot, message: FacebookMessage) {
    if (message.payload === 'humanhandover') {
        facebookConnector.handOverToSecondaryApp(message.user);
        bot.reply(message, intentDetector.getIntentForTag(message.payload).getRandomResponse());
    }
});

controller.on('facebook_referral', async function (bot: FacebookBot, message: FacebookMessage) {
    if (message.referral.ref === 'morningslot' || message.referral.ref === 'afternoonslot') {
        bot.reply(message, getGenericResponse(intentDetector.getIntentForTag(message.referral.ref).getRandomResponse()));
    }
});

// serve the website for the moped store
controller.webserver.set("views", path.join(__dirname, "/views"));
controller.webserver.set("view engine", "pug");

controller.webserver.get("/", (req, res) => {
    res.render('index', {
        page_id: process.env.FACEBOOK_PAGE_ID
    });
});

// create a response that renders to generic template in facebook
function getGenericResponse(text: string) : {notification_type: 'REGULAR',attachment: FacebookAttachment } {
    return {
        notification_type: 'REGULAR',
        attachment: new GenericTemplate(
            text,
            [new PostbackButton('Customer Agent', 'humanhandover'), new ShareButton]).resolve()
    }
}

// handle basic setup for messenger plugin (beta)
facebookConnector.whiteListURIs([process.env.WEBSITE_URI]);