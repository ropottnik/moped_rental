import {IntentCandidate} from './Interfaces/IntentServerResponse';
import * as fs from 'fs';
import { RawIntent } from "./Interfaces/RawIntent";
import {Intent} from "./Intent";

export class IntentDetector {
    context : any;
    intents: RawIntent[];

    constructor(intentsFilePath: string) {
        this.context = {};
        this.intents = JSON.parse(fs.readFileSync(intentsFilePath, "utf8")).intents
    }

    detectIntentForUserAndHandleContext(userId: string, intentList: IntentCandidate[]) : Intent {
        if (intentList) {
            // loop as long as there are matches to process
            for (let nextBestCandidate of intentList) {
                let intent = this.getIntentForTag(nextBestCandidate.tag);

                // set context for this tag if necessary
                if (intent.context_set || intent.context_set === "") {
                    this.context[userId] = intent.context_set
                }

                if (!intent.context_filter || (this.context[userId] && intent.context_filter === this.context[userId])) {
                    // a random response from the tag
                    // return tag.responses[Math.floor(Math.random()*tag.responses.length)];
                    return intent;
                }
            }
        }
        return null;
    }

    getIntentForTag(tag: string) : Intent {
        for (let intent of this.intents) {
            if (tag === intent.tag) {
                return new Intent(intent);
            }
        }
    }
}