'use strict';
import axios from 'axios';
import {IntentCandidate} from './Interfaces/IntentServerResponse';

export class IntentServerConnector {
    host: string;
    port: string;

    constructor(host: string, port: string) {
        this.host = host;
        this.port = port;
    }

    get serverConnection () : string {
        return `http://${this.host}:${this.port}`;
    }

    async getIntents(sentence: string) : Promise<IntentCandidate[]> {
        return (await axios.post(
            this.serverConnection,
            {
                sentence: sentence
            }
        )).data;
    }
}