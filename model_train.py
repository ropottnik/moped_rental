##########################################################################################
# "Training and exporting a simple tag recognition model"
#
#  Inspired by https://github.com/teddius/TensorFlow_Chatbot_for_20171128_Talk_BotsHub_Meetup_Vienna
##########################################################################################

######################################################################
# General utilities
######################################################################
import os, sys

######################################################################
# Things we need for NLP
######################################################################
import nltk
from nltk.stem.lancaster import LancasterStemmer
stemmer = LancasterStemmer() # english stemmer

######################################################################
# things we need for Tensorflow
######################################################################
import tensorflow as tf
import random
import numpy as np
from keras import metrics, optimizers
from keras.callbacks import ModelCheckpoint, TensorBoard, EarlyStopping
from keras.layers import Dense, Flatten, Conv1D, Embedding, MaxPooling1D, Dropout
from keras.models import Sequential
from sklearn.model_selection import train_test_split

######################################################################
# things we need for Saving a model###################################
######################################################################
from tensorflow.python.saved_model import builder as saved_model_builder
from tensorflow.python.saved_model import tag_constants, signature_constants, signature_def_utils_impl
from keras import backend as K

######################################################################
# saving configuration
######################################################################

export_path_base = os.getcwd() + "/saved_models"
model_version = 2

######################################################################
# Import our chat-bot intents file
######################################################################
import json
with open('intents.json') as json_data:
    intents = json.load(json_data)

def main():
    if model_version <= 0:
        print('Please specify a positive value for version number.')
        sys.exit(-1)

    ######################################################################
    # Let's start to build our training data
    ######################################################################
    words = []
    classes = []
    documents = []
    ignore_words = ['?']

    # loop through each sentence in our intents patterns
    for intent in intents['intents']:
        for pattern in intent['patterns']:

            # tokenize each word in the sentence
            w = nltk.word_tokenize(pattern)

            # add to our words list
            words.extend(w)

            # add to documents in our corpus
            documents.append((w, intent['tag']))

            # add to our classes list
            if intent['tag'] not in classes:
                classes.append(intent['tag'])

    # stem and lower each word and remove duplicates
    words = [stemmer.stem(w.lower()) for w in words if w not in ignore_words]
    words = sorted(list(set(words)))

    # remove duplicates
    classes = sorted(list(set(classes)))

    # create our training data
    training = []
    output = []
    # create an empty array for our output
    output_empty = [0] * len(classes)

    # training set, bag of words for each sentence
    for doc in documents:
        # initialize our bag of words
        bag = []
        # list of tokenized words for the pattern
        pattern_words = doc[0]
        # stem each word
        pattern_words = [stemmer.stem(word.lower()) for word in pattern_words]
        # create our bag of words array
        for w in words:
            bag.append(1) if w in pattern_words else bag.append(0)

        # output is a '0' for each tag and '1' for current tag
        output_row = list(output_empty)
        output_row[classes.index(doc[1])] = 1

        training.append([bag, output_row])

    # print('training: ' + str(training))

    # shuffle our features and turn into np.array
    random.shuffle(training)
    training = np.array(training)

    # create train and test lists
    X = list(training[:, 0])
    y = list(training[:, 1])

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=32, shuffle=True)

    # reset underlying graph data
    tf.reset_default_graph()

    ####################################################################################
    # Build a very simple neural network
    ####################################################################################
    model = Sequential()
    model.add(Dense(100, activation="relu",input_dim=(np.array(X_train).shape[1])))
    model.add(Dense(50, activation='relu'))
    model.add(Dense(len(classes), activation='softmax'))

    # metrics
    adam = optimizers.Adam(lr=0.1, decay=0.005)
    model.compile(loss='categorical_crossentropy',
                      optimizer=adam,
                      metrics=['accuracy'])

    # Callbacks for the evaluation of the model
    early_stop = EarlyStopping(monitor='val_loss', min_delta=0, patience=10, verbose=0, mode='auto')
    checkpoint = ModelCheckpoint('./weights-improvement-{epoch:02d}-{loss:.4f}.hdf5',
                                 monitor='loss', verbose=1, save_best_only=True, mode='min')
    #callbacks_list = [checkpoint, tensorboard_callback, early_stop]
    callbacks_list = [early_stop]

    nr_of_epoches=100
    batch_size=32
    history = model.fit(X_train,
                        y_train,
                        epochs=nr_of_epoches,
                        batch_size=batch_size,
                        validation_data=(X_test, y_test),
                        callbacks=callbacks_list)

    prediction_signature = tf.saved_model.signature_def_utils.predict_signature_def({"bow": model.input}, {"prediction":model.output})

    export_path = os.path.join(
        tf.compat.as_bytes(export_path_base),
        tf.compat.as_bytes(str(model_version)))

    builder = saved_model_builder.SavedModelBuilder(export_path)
    sess = K.get_session()

    #############################################################
    # Add the meta_graph and the variables to the builder
    #############################################################
    builder.add_meta_graph_and_variables(
        sess, [tag_constants.SERVING],
        signature_def_map={
            signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY:
                prediction_signature,
        })
    #############################################################
    # save the graph
    #############################################################
    builder.save()

if __name__ == '__main__':
    main()
