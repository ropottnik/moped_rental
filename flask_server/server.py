import json
from text_processor import TextProcessor
from tf_connector import TFConnector
from flask import Flask, request
app = Flask(__name__)

processor = TextProcessor('intents.json')
connector = TFConnector('tfserve', 9000)


@app.route('/', methods=['POST'])
def process_text():
    request_data = json.loads(request.data.decode('utf-8'))
    sentence = request_data['sentence']
    bow = processor.bow(sentence)
    results = connector.predict(bow)
    classes = processor.classify(results)
    return json.dumps(classes)
