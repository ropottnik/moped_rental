import json
import random

import nltk
import numpy as np
from nltk.stem.lancaster import LancasterStemmer


class TextProcessor:
    # set everything up
    def __init__(self, intent_path):
        self.stemmer = LancasterStemmer()  # english stemmer
        self.words = []
        self.classes = []
        self.documents = []
        self.ignore_words = ['?']
        self.ERROR_THRESHOLD = 0.00000000001
        self.context = {}

        with open(intent_path) as json_data:
            self.intents = json.load(json_data)

        # loop through each sentence in our intents patterns
        for intent in self.intents['intents']:
            for pattern in intent['patterns']:

                # tokenize each word in the sentence
                w = nltk.word_tokenize(pattern)

                # add to our words list
                self.words.extend(w)

                # add to documents in our corpus
                self.documents.append((w, intent['tag']))

                # add to our classes list
                if intent['tag'] not in self.classes:
                    self.classes.append(intent['tag'])

        # stem and lower each word and remove duplicates
        self.words = [self.stemmer.stem(w.lower()) for w in self.words if w not in self.ignore_words]
        self.words = sorted(list(set(self.words)))

        # remove duplicates
        self.classes = sorted(list(set(self.classes)))

    def clean_up_sentence(self, sentence):
        # tokenize the pattern
        sentence_words = nltk.word_tokenize(sentence)
        # stem each word
        sentence_words = [self.stemmer.stem(word.lower()) for word in sentence_words]
        return sentence_words

    # return bag of words array: 0 or 1 for each word in the bag that exists in the sentence
    def bow(self, sentence, show_details=False):
        # tokenize the pattern
        sentence_words = self.clean_up_sentence(sentence)
        # bag of words
        bag = [0] * len(self.words)
        for s in sentence_words:
            for i, w in enumerate(self.words):
                if w == s:
                    bag[i] = 1
                    if show_details:
                        print("found in bag: %s" % w)

        return np.array(bag)

    def classify(self, pb_response):
        # filter out predictions below a threshold
        results = [[i, r] for i, r in enumerate(pb_response) if r > self.ERROR_THRESHOLD]
        # sort by strength of probability
        results.sort(key=lambda x: x[1], reverse=True)
        return_list = []
        for r in results:
            return_list.append({"tag": self.classes[r[0]], "score": r[1]})
        # return tuple of tag and probability
        return return_list