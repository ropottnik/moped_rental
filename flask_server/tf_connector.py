import numpy as np
import tensorflow as tf
from grpc.beta import implementations
from tensorflow_serving.apis import predict_pb2

from tensorflow_serving.apis import prediction_service_pb2


class TFConnector:
    def __init__(self, host, port):
        channel = implementations.insecure_channel(host, int(port))
        self.stub = prediction_service_pb2.beta_create_PredictionService_stub(channel)

    def predict(self, data):
        request = predict_pb2.PredictRequest()
        request.model_spec.name = 'moped'
        request.inputs['bow'].CopyFrom(
            tf.contrib.util.make_tensor_proto(np.array([data]), dtype=tf.float32))
        return self.stub.Predict(request, 10.0).outputs['prediction'].float_val